package com.mindvalley_hassan_android_test.Controller;

import android.app.Application;
import android.provider.ContactsContract;

import com.mindvalley_hassan_android_test.Loader.DataLoader;
import com.mindvalley_hassan_android_test.Models.Post;
import com.mindvalley_hassan_android_test.Models.User;

import java.util.ArrayList;

/**
 * Created by hassa on 11/12/2016.
 */

public class Controller extends Application {
    private DataLoader dataLoader;

    private ArrayList<Post> posts = new ArrayList<>();
    private User user = new User();

    public ArrayList<Post> getPosts() {
        return posts;
    }

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DataLoader getDataLoader() {
        return dataLoader;
    }

    public void setDataLoader(DataLoader dataLoader) {
        this.dataLoader = dataLoader;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        dataLoader = new DataLoader(getApplicationContext());
    }
}