package com.mindvalley_hassan_android_test.Cache;

import android.graphics.Bitmap;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by hassa on 11/12/2016.
 */

public class MemoryCache {

    private static final int BITMAP =0;
    private static final int DATA =1;

    private int type;

    private static final String TAG = "MemoryCache";

    private Map<String, Object> cache = Collections.synchronizedMap(
            new LinkedHashMap<String, Object>(10,1.5f,true));

    private long size=0;

    private long limit=1000000;

    public MemoryCache(int type){
        this.type = type;
        setLimit(Runtime.getRuntime().maxMemory()/4);
    }

    public void setLimit(long new_limit){

        limit=new_limit;
        Log.i(TAG, "MemoryCache will use up to "+limit/1024./1024.+"MB");
    }

    public Object get(String key){
        try{
            if(!cache.containsKey(key))
                return null;

            return cache.get(key);

        }catch(NullPointerException ex){
            ex.printStackTrace();
            return null;
        }
    }

    public void put(String key, Object data){
        try{
            if(cache.containsKey(key))
                size-=getSizeInBytes(cache.get(key));
            cache.put(key, data);
            size+=getSizeInBytes(data);
            checkSize();
        }catch(Throwable th){
            th.printStackTrace();
        }
    }

    private void checkSize() {
        Log.i(TAG, "cache size="+size+" length="+cache.size());
        if(size>limit){
            Iterator<Map.Entry<String, Object>> iterator=cache.entrySet().iterator();

            while(iterator.hasNext()){
                Map.Entry<String, Object> entry=iterator.next();
                size-=getSizeInBytes(entry.getValue());
                iterator.remove();
                if(size<=limit)
                    break;
            }
            Log.i(TAG, "Clean cache. New size "+cache.size());
        }
    }

    public void clear() {
        try{
            cache.clear();
            size=0;
        }catch(NullPointerException ex){
            ex.printStackTrace();
        }
    }

    long getSizeInBytes(Object data) {
        if (data == null){
            return 0;
        }
        switch (type){
            case BITMAP:{
                Bitmap bitmap = (Bitmap) data;
                return bitmap.getRowBytes() * bitmap.getHeight();
            }
            case DATA:{
                String string = (String) data;
                try {
                    return string.getBytes("UTF-8").length;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        return 0;
    }
}