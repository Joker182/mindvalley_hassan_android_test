package com.mindvalley_hassan_android_test;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.mindvalley_hassan_android_test.Controller.Controller;
import com.mindvalley_hassan_android_test.Loader.DataLoader;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String response ="";
        final Controller controller = (Controller) getApplicationContext();
        controller.getDataLoader().loadDatafromUrl("http://pastebin.com/raw/wgkJgazE",null,response);

        ImageView image = (ImageView)findViewById(R.id.image);
        controller.getDataLoader().loadDatafromUrl("https://images.unsplash.com/photo-1464550883968-cec281c19761?ixlib=rb-0.3.5\\u0026q=80\\u0026fm=jpg\\u0026crop=entropy\\u0026w=200\\u0026fit=max\\u0026s=9fba74be19d78b1aa2495c0200b9fbce",image,null);
    }
}
