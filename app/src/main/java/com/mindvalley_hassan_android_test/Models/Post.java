package com.mindvalley_hassan_android_test.Models;

/**
 * Created by hassa on 11/12/2016.
 */

public class Post {

    private String id;
    private String created_at;
    private String width;
    private String height;
    private String color;
    private String likes;
    private String liked_by_user;
    private User user;
    private Object[] current_user_collections;
    private Urls urls;
    private Category[] categories;
    private Links links;
}
