package com.mindvalley_hassan_android_test.Loader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;
import com.mindvalley_hassan_android_test.Cache.MemoryCache;
import com.mindvalley_hassan_android_test.Models.Post;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by hassa on 11/13/2016.
 */

public class ResponseLoader {

    MemoryCache memoryCache = new MemoryCache(1);

    private Map<String, String> responses = Collections.synchronizedMap(
            new WeakHashMap<String, String>());
    ExecutorService executorService;

    Handler handler = new Handler();

    public ResponseLoader(Context context){
        executorService= Executors.newFixedThreadPool(5);
    }

    public void loadDataToResponse(String url, String response){
        responses.put(response,url);
        String data = (String) memoryCache.get(url);
        if(data!=null){
            response = data;
        }else{
            queueData(url, response);
        }
    }

    private void queueData(String url,String response){
        DataToLoad d = new DataToLoad(url, response);
        executorService.submit(new DataLoader(d));
    }

    private class DataToLoad{
        public String url;
        public String response;

        public DataToLoad(String u, String r){
            url=u;
            response=r;
        }
    }

    class DataLoader implements Runnable {
        DataToLoad dataToLoad;

        DataLoader(DataToLoad dataToLoad){
            this.dataToLoad=dataToLoad;
        }

        @Override
        public void run() {
            try{
                String response = getResponse(dataToLoad.url);
                memoryCache.put(dataToLoad.url, response);
                LoadingResponse bd=new LoadingResponse(response, dataToLoad);
                handler.post(bd);
            }catch(Throwable th){
                th.printStackTrace();
            }
        }
    }

    class LoadingResponse implements Runnable{
        String data;
        DataToLoad dataToLoad;
        public LoadingResponse(String b, DataToLoad p){data=b;dataToLoad=p;}
        public void run(){
            dataToLoad.response = data;
            Log.d("Data",data);
        }
    }

    public String getResponse(String url){
        try {
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is=conn.getInputStream();
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();
            String temp;
            while ((temp = streamReader.readLine()) != null)
                responseStrBuilder.append(temp);
            return responseStrBuilder.toString();
        } catch (Throwable ex){
            ex.printStackTrace();
            if(ex instanceof OutOfMemoryError)
                memoryCache.clear();
            return null;
        }
    }

    public void clearCache() {
        memoryCache.clear();
    }
}
