package com.mindvalley_hassan_android_test.Loader;

import android.content.Context;
import android.widget.ImageView;

import com.mindvalley_hassan_android_test.Models.Post;

/**
 * Created by hassa on 11/13/2016.
 */

public class DataLoader {

    private Context context;

    public DataLoader(Context context){
        this.context = context;
    }

    ImageLoader imageLoader = new ImageLoader(context);
    ResponseLoader dataLoader = new ResponseLoader(context);

    public void loadDatafromUrl(String url, ImageView imageView, String response){
        if (imageView == null){
            dataLoader.loadDataToResponse(url,response);
        }else if (response==null){
            imageLoader.displayImage(url,imageView);
        }
    }
}
